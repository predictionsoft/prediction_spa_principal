# Fase de construcción
FROM node:16 as build-stage

WORKDIR /app

COPY package*.json /app/

RUN npm install -g @ionic/cli

RUN npm install

COPY ./ /app/

RUN ionic build

# Fase de producción
FROM nginx:1.17.1-alpine as production-stage

COPY --from=build-stage /app/www/ /usr/share/nginx/html

# Copia la configuración de nginx personalizada
COPY ./nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
