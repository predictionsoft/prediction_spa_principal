import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'prediction_spa_principal',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  }
};

export default config;
