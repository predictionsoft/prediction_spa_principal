import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'monthName' })
export class MonthNamePipe implements PipeTransform {
  private monthNames: string[] = [
    'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
  ];

  transform(monthNumber: number): string {
    return this.monthNames[monthNumber - 1] || '';
  }
}
