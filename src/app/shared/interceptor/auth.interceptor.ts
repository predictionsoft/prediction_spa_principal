import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders,
  HttpErrorResponse,
  HttpStatusCode,
} from '@angular/common/http';
import { Observable, catchError, map, tap, throwError } from 'rxjs';
import { Storage } from './../../services/auth/storage';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private _router: Router) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    const clonedReq = request.clone({
      headers: new HttpHeaders({
        Authorization: `Bearer ${Storage.instance.getLocalStoreToken()}`,
      }),
    });

    return next.handle(clonedReq).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.status === HttpStatusCode.Unauthorized) {
          this._router.navigate(['/logout']);
        }
        return throwError(() => error);
      })
    );
  }
}
