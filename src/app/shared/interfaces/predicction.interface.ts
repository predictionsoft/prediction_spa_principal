export interface IFiles {
  month: number;
  year: string;
  createdAt: Date;
  createdBy: string;
}
