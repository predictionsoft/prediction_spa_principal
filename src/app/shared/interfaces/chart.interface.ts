export interface IChartInterface {
    chart_id: string;
    height: 'small' | 'medium';
    colSpan: number;
}
