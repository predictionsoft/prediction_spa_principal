import { CustomAlertService } from './../../services/shared/alert.service';
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FileService } from 'src/app/services/http/backend/file.service';
import { IFiles } from 'src/app/shared/interfaces/predicction.interface';
import { LoadingController } from '@ionic/angular';
import { ToastService } from 'src/app/services/shared/toast.service';
@Component({
  selector: 'app-tab-prediction',
  templateUrl: 'tab-prediction.page.html',
  styleUrls: ['tab-prediction.page.scss'],
})
export class TabPredictionPage {
  uploadedFiles: IFiles[] = [];
  monthOptions: { value: number; label: string }[] = [];
  yearOptions: { value: number; label: string }[] = [];
  selectedMonth: number = 0;
  selectedYear: number = 0;
  selectedFile!: File;

  constructor(
    private readonly fileService: FileService,
    private http: HttpClient,
    private loadingController: LoadingController,
    private toastService: ToastService,
    private customerAlert: CustomAlertService
  ) {}
  ngOnInit() {
    this.loadOptions();
    this.getFilesHistory();
  }

  loadOptions() {
    this.http.get<any>('assets/json/dates.json').subscribe((data) => {
      this.monthOptions = data.monthOptions;
      this.yearOptions = data.yearOptions;
    });
  }

  async getFilesHistory() {
    const loading = await this.presentLoading('');
    this.fileService.getFileHistory().subscribe({
      next: (value: IFiles[]) => {
        this.uploadedFiles = value;
        loading.dismiss();
      },
      error: (err) => {
        console.log('err', err);
      },
    });
  }

  handleSelectedValueChangeYear(value: number) {
    this.selectedYear = value;
  }

  handleSelectedValueChangeMonth(value: number) {
    this.selectedMonth = value;
  }

  handleFile(file: File) {
    this.selectedFile = file;
  }

  async uploadFile() {
    if (this.selectedFile && this.selectedMonth && this.selectedYear) {
      const loading = await this.presentLoading('Subiendo archivo...');
      this.fileService
        .uploadFile(this.selectedFile, this.selectedMonth, this.selectedYear)
        .subscribe({
          next: async (value: any) => {
            console.log('Response', value);
            if (value.success === true) {
              await this.getFilesHistory();
              loading.dismiss();
              await this.toastService.presentToast(
                'Archivo subido exitosamente',
                'success'
              );
            } else {
              await this.toastService.presentToast(
                value.errorMessage.title,
                'danger'
              );
              loading.dismiss();
            }
          },
          error: async (error) => {
            console.log('Error al subir el archivo', error);
            await this.toastService.presentToast(
              'Error al subir el archivo',
              'danger'
            );
            loading.dismiss();
          },
        });
    } else {
      this.customerAlert.presentAlert(
        'Información',
        'Debe seleccionar un archivo y especificar el mes y el año.',
        'success'
      );
    }
  }
  async deleteFiles() {
    const loading = await this.presentLoading('Eliminando archivos...');
    this.fileService.deleteFiles().subscribe({
      next: async (value) => {
        await this.getFilesHistory();
        loading.dismiss();
        await this.toastService.presentToast(
          'Archivos eliminados exitosamente',
          'success'
        );
      },
      error: async (error) => {
        await this.toastService.presentToast(
          'Error al eliminar el archivo',
          'danger'
        );
        loading.dismiss();
      },
    });
  }

  async predictSales() {
    const loading = await this.presentLoading('Proceso iniciado...');
    this.fileService.predictSales().subscribe({
      next: async (value) => {
        await this.getFilesHistory();
        loading.dismiss();
        await this.toastService.presentToast(
          'Proceso terminado exitosamente',
          'success'
        );
      },
      error: async (error) => {
        await this.toastService.presentToast('Error en el proceso', 'danger');
        loading.dismiss();
      },
    });
  }
  async presentLoading(message: string) {
    const loading = await this.loadingController.create({
      message: message,
      duration: 0,
    });
    await loading.present();
    return loading;
  }
}
