import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ExploreContainerComponentModule } from '../../components/explore-container/explore-container.module';
import { DateSelectComponent } from 'src/app/components/date-select/date-select.component';
import { TabPredictionPage } from './tab-prediction.page';
import { TabPredictionPageRoutingModule } from './tab-prediction-routing.module';
import { FileUploadComponent } from 'src/app/components/file-upload/file-upload.component';
import { MonthNamePipe } from 'src/app/shared/pipes/month-name-pipe';
import { ToastService } from 'src/app/services/shared/toast.service';
import { CustomAlertService } from 'src/app/services/shared/alert.service';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    TabPredictionPageRoutingModule,
  ],
  declarations: [TabPredictionPage, DateSelectComponent, FileUploadComponent, MonthNamePipe],
  providers: [ToastService, CustomAlertService]
})
export class TabPredictionPageModule {}
