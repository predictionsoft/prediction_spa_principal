import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabPredictionPage } from './tab-prediction.page';

const routes: Routes = [
  {
    path: '',
    component: TabPredictionPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabPredictionPageRoutingModule {}
