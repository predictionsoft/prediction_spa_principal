import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import { AuthGuard } from '@auth0/auth0-angular';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        loadChildren: () =>
          import('../tab-dashboard/tab-dashboard.module').then(
            (m) => m.TabDashboardPageModule
          ),
      },
      {
        path: 'prediction',
        loadChildren: () =>
          import('../tab-prediction/tab-prediction.module').then(
            (m) => m.TabPredictionPageModule
          ),
      },
      {
        path: 'perfil',
        loadChildren: () =>
          import('../tab-perfil/tab-perfil.module').then(
            (m) => m.TabPerfilPageModule
          ),
      },
      {
        path: '',
        redirectTo: '/tab/dashboard',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
