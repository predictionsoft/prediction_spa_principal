import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@auth0/auth0-angular';
import { Browser } from '@capacitor/browser';
import { Base } from 'src/app/services/auth/base';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage extends Base implements OnInit {
  token$ = this.auth.idTokenClaims$.subscribe((claims: any) => {
    if (claims !== null && '__raw' in claims) {
      const token = claims.__raw;
      this.storage.saveToken(token);
      this.router.navigate(['/tab']);
    }
  });

  constructor(public auth: AuthService, private router: Router) {
    super();
  }

  ngOnInit() {}
  login() {
    this.auth
      .loginWithRedirect({
        async openUrl(url: string) {
          return Browser.open({ url, windowName: '_self' });
        },
      })
      .subscribe();
  }
}
