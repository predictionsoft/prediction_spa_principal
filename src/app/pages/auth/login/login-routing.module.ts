import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginPage } from './login.page';
import { IsSignedInGuard } from 'src/app/shared/guards/is-signed-in.guard';

const routes: Routes = [
  {
    path: '',
    component: LoginPage,
    canActivate: [IsSignedInGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginPageRoutingModule {}
