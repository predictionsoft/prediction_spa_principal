import { Component, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { callbackUri } from '../../../services/auth/auth.config';
import { Browser } from '@capacitor/browser';
import { SessionService } from 'src/app/services/auth/session.service';

@Component({
  selector: 'app-logout',
  template: '',
})
export class LogoutPage implements OnInit {
  constructor(public auth: AuthService, private session: SessionService) {}

  ngOnInit() {
    this.logout();
  }
  logout() {
    this.auth
      .logout({
        logoutParams: {
          returnTo: callbackUri,
        },
        async openUrl(url: string) {
          return Browser.open({ url, windowName: '_self' });
        },
      })
      .subscribe(() => {
        this.session.destroySession();
      });
  }
}
