import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TabPerfilPage } from './tab-perfil.page';
import { ExploreContainerComponentModule } from '../../components/explore-container/explore-container.module';
import { TabPerfilPageRoutingModule } from './tab-perfil-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    TabPerfilPageRoutingModule,
  ],
  declarations: [TabPerfilPage],
  providers: [],
})
export class TabPerfilPageModule {}
