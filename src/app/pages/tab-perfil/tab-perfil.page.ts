import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/http/backend/user.service';

@Component({
  selector: 'app-tab-perfil',
  templateUrl: 'tab-perfil.page.html',
  styleUrls: ['tab-perfil.page.scss'],
})
export class TabPerfilPage implements OnInit {
  responseUser: any;
  loading: boolean = true;
  constructor(
    private readonly userService: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers() {
    this.userService.getUser().subscribe({
      next: (res) => {
        this.responseUser = res;
        this.loading = false;
      },
      error: (err) => {
        console.log(err);
      },
    });
  }

  logout() {
    this.router.navigate(['/logout']);
  }
}
