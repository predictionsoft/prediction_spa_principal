import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TabDashboardPage } from './tab-dashboard.page';
import { ExploreContainerComponentModule } from '../../components/explore-container/explore-container.module';

import { TabDashboardPageRoutingModule } from './tab-dashboard-routing.module';
import { ToastService } from 'src/app/services/shared/toast.service';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    TabDashboardPageRoutingModule,
  ],
  declarations: [TabDashboardPage],
  providers: [ToastService],
})
export class TabDashboardPageModule {}
