import {
  Component,
  ElementRef,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import ChartsEmbedSDK, { Chart } from '@mongodb-js/charts-embed-dom';
import { ChartsService, IChart } from '../../services/chart/charts.service';
import { environment } from 'src/environments/environment';
import { ToastService } from 'src/app/services/shared/toast.service';
import { DashboardService } from 'src/app/services/chart/dahsboard.service';

interface ChartComponent extends IChart {
  chart?: Chart;
  filter?: any;
}

@Component({
  selector: 'app-tab-dashboard',
  templateUrl: 'tab-dashboard.page.html',
  styleUrls: ['tab-dashboard.page.scss'],
})
export class TabDashboardPage implements OnInit {
  sdk: ChartsEmbedSDK = new ChartsEmbedSDK({
    baseUrl: environment.chartsClientSDK,
  });
  charts: Array<ChartComponent> = [];
  chartsLoading = true;

  @ViewChildren('chart') chartDivs!: QueryList<ElementRef<HTMLDivElement>>;
  constructor(
    private chartsService: ChartsService,
    private toastService: ToastService,
    private dashboardService: DashboardService
  ) {
    this.getDashboardService();
  }

  async ngOnInit() {
    try {
      this.chartsLoading = true;
      this.charts = await this.chartsService.getCharts();
      await new Promise((resolve) => setTimeout(resolve, 500));
      this.charts.forEach(async (element, key) => {
        this.charts[key].chart = this.createChart(element.chart_id);
        const chartDivs = this.chartDivs.get(key);
        if (chartDivs) {
          await this.charts[key].chart?.render(chartDivs.nativeElement);
        }
        this.chartsLoading = false;
      });
    } catch (error) {
      await this.toastService.presentToast('Error generate dashboard', 'error');
    }
  }

  createChart(chartId: string) {
    return this.sdk.createChart({ chartId });
  }

  async getDashboardService() {
    this.dashboardService.getChartsClient().subscribe({
      next: (res) => {
        this.charts = res;
      },
      error: async (err) => {
        console.log(err);
        await this.toastService.presentToast(
          'Error servicio dashboard',
          'error'
        );
      },
    });
  }
}
