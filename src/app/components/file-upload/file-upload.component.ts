import {
  Component,
  ViewChild,
  ElementRef,
  Output,
  EventEmitter,
} from '@angular/core';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
})
export class FileUploadComponent {

  @Output() addFile: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}

  selectedFile(event: Event) {
    const fileInputElement: HTMLInputElement = (event.target as HTMLInputElement);
    if (fileInputElement.files && fileInputElement.files.length > 0) {
      this.addFile.emit(fileInputElement.files[0]);
    }
  }

}
