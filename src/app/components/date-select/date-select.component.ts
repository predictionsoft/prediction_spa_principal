import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-date-select',
  templateUrl: './date-select.component.html',
  styleUrls: ['./date-select.component.scss'],
})
export class DateSelectComponent {
  @Input() label!: string;
  @Input() name!: string;
  @Input() options!: { value: number; label: string }[];
  @Input() selectedValue!: number;
  @Output() selectedValueChange: EventEmitter<number> =
    new EventEmitter<number>();

  onValueChange(event: any) {
    const value: number = event.detail.value;
    if (!isNaN(value)) {
      this.selectedValue = value;
      this.selectedValueChange.emit(value);
    }
  }
}
