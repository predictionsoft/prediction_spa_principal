import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable()
export class CustomAlertService {
  constructor(private alertController: AlertController) {}

  async presentAlert(title: string, message: string, color: string) {
    const alert = await this.alertController.create({
      header: title,
      message: message,
      cssClass: color,
      buttons: ['OK'],
    });

    await alert.present();
  }
}
