import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable()
export class ToastService {
  constructor(private toastController: ToastController) {}

  async presentToast(
    message: string,
    color: string,
    header: string | undefined = undefined,
    duration: number = 2000
  ) {
    const toast = await this.toastController.create({
      header: header,
      message: message,
      color: color,
      duration: duration,
    });
    toast.present();
  }
}
