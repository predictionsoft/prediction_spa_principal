import { Injectable } from '@angular/core';

export interface IChart {
  chart_id: string;
  height: 'small' | 'medium';
  colSpan: number;
}

@Injectable({
  providedIn: 'root',
})
export class ChartsService {
  charts: IChart[] = [];

  getCharts(): Promise<IChart[]> {
    return Promise.resolve(this.charts);
  }
}
