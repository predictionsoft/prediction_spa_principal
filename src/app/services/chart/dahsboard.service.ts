import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IChartInterface } from 'src/app/shared/interfaces/chart.interface';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  constructor(private _http: HttpClient) {}

  public getChartsClient(): Observable<IChartInterface[]> {
    const url = `assets/dashboard-service.json`;
    return this._http.get<IChartInterface[]>(url);
  }
}
