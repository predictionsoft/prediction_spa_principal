import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IFiles } from 'src/app/shared/interfaces/predicction.interface';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root',
})
export class FileService {
  constructor(private http: HttpClient) {}

  uploadFile(file: File, mes: number, anio: number) {
    const formData = new FormData();
    formData.append('file', file, file.name);
    formData.append('month', mes.toString());
    formData.append('year', anio.toString());
    console.log('formData', formData);

    return this.http.post(`${environment.backend}/learning`, formData);
  }

  getFileHistory(): Observable<IFiles[]> {
    return this.http.get(`${environment.backend}/learning/records`).pipe(
      map((data: any) => {
        return data.map((item: any) => ({
          month: item.month as number,
          year: item.year as string,
          createdAt: new Date(item.createdAt) as Date,
          createdBy: item.createdBy as string,
        }));
      })
    );
  }

  deleteFiles() {
    return this.http.delete(`${environment.backend}/learning`);
  }

  predictSales() {
    return this.http.get(`${environment.backend}/prediction`);
  }
}
