import { User } from '@auth0/auth0-angular';
import { Storage } from './storage';

export class Base {
  protected token: User | null;
  protected storage: Storage;

  constructor() {
    this.token = Storage.instance.getToken();
    this.storage = Storage.instance;
  }
}
