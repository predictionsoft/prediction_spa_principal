import { User } from '@auth0/auth0-angular';
import { Jwt } from './jwt';

const TOKEN_KEY = 'AhXbkvT5W6';

export class Storage {
  private static _instance: Storage;

  private constructor() {}

  static get instance(): Storage {
    if (!this._instance) {
      this._instance = new Storage();
    }
    return this._instance;
  }

  public removeToken(): void {
    localStorage.clear();
  }

  public saveToken(token: string): void {
    localStorage.removeItem(TOKEN_KEY);
    localStorage.setItem(TOKEN_KEY, token);
  }

  public token() {
    return localStorage.getItem(TOKEN_KEY);
  }

  public getToken(): User | null {
    const token = localStorage.getItem(TOKEN_KEY);
    if (token) {
      return Jwt.instance.decode(token);
    }
    return null;
  }

  public getLocalStoreToken(): string | null {
    const token = localStorage.getItem(TOKEN_KEY);
    if (token) {
      return token;
    }
    return null;
  }
}
