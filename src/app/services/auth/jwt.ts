import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from '@auth0/auth0-angular';

export class Jwt {
  private static _instance: Jwt;

  private constructor() {}

  static get instance(): Jwt {
    if (!this._instance) {
      this._instance = new Jwt();
    }
    return this._instance;
  }

  public decode(token: string): User | null {
    const helper = new JwtHelperService();
    return helper.decodeToken(token);
  }

  public valid(token: string): boolean {
    const helper = new JwtHelperService();
    return helper.isTokenExpired(token);
  }
}
