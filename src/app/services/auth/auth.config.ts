import { isPlatform } from '@ionic/angular';
import config from 'capacitor.config';
import { environment } from 'src/environments/environment';

export const domain = environment.domain;
export const clientId = environment.clientId;
const { appId } = config;

const auth0Domain = domain;
const iosOrAndroid = isPlatform('hybrid');

export const callbackUri = iosOrAndroid
  ? `${appId}://${auth0Domain}/capacitor/${appId}/callback`
  : environment.host;
