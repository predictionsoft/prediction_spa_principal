import { Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Storage } from './storage';

@Injectable({
  providedIn: 'root',
})
export class SessionService implements OnInit {
  constructor(private _router: Router) {}

  ngOnInit(): void {}

  public isLoggedIn(): boolean {
    return Storage.instance.getToken() !== null;
  }

  public destroySession(): void {
    Storage.instance.removeToken();
    if (!this.isLoggedIn()) {
      this._router.navigate(['/login']);
    }
  }
}
